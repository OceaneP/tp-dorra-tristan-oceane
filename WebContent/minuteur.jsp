<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Minuteur</title>
    <link rel="stylesheet" href="style_cuisine.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</head>

<body class="body">
    <header class="header">
            <nav class= "navbar navbar-expand-lg navbar-light bg-light">
            <img class = "img_gauche" width="150px" src="https://i.pinimg.com/originals/b1/9b/79/b19b7971a77bcbe56edcca0bb6dea5dc.jpg" alt="Miam">
           
            <a class="navbar-brand" href="accueil.html">Accueil</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav">
                <li class="nav-item">
                  <a class="nav-link" href="decongelation">Guide de décongélation</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="conversion">Guide de conversion d'unités</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="minuteur.jsp">Minuteur</a>
                </li>
                
              </ul>
            </div>           
            <img class = "img_droite" width="150px" src="https://www.feter-recevoir.com/upload/image/20-serviettes-vintage-patisserie-33x33cm-p-image-181854-grande.jpg" alt="Gateaux"> 
            </nav>
    </header>  
<div class="timer-display">

	<p id="timer"></p>
</div>

<body>

	<div class="main">
	
		<label for="hours">Hours</label>
		<label for="minutes">Minutes</label>
		<label for="seconds">Seconds</label>

		<br>
<form>
		<select name="hours" id="hours">
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			<option value="6">6</option>
			<option value="7">7</option>
			<option value="8">8</option>
			<option value="9">9</option>
			<option value="10">10</option>
			<option value="11">11</option>
			<option value="12">12</option>
			<option value="13">13</option>
			<option value="14">14</option>
			<option value="15">15</option>
			<option value="16">16</option>
			<option value="17">17</option>
			<option value="18">18</option>
			<option value="19">19</option>
			<option value="20">20</option>
			<option value="21">21</option>
			<option value="22">22</option>
			<option value="23">23</option>
		</select>

		<select name="minutes" id="minutes">
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			<option value="6">6</option>
			<option value="7">7</option>
			<option value="8">8</option>
			<option value="9">9</option>
			<option value="10">10</option>
			<option value="11">11</option>
			<option value="12">12</option>
			<option value="13">13</option>
			<option value="14">14</option>
			<option value="15">15</option>
			<option value="16">16</option>
			<option value="17">17</option>
			<option value="18">18</option>
			<option value="19">19</option>
			<option value="20">20</option>
			<option value="21">21</option>
			<option value="22">22</option>
			<option value="24">24</option>
			<option value="25">25</option>
			<option value="26">26</option>
			<option value="27">27</option>
			<option value="28">28</option>
			<option value="29">29</option>
			<option value="30">30</option>
			<option value="31">31</option>
			<option value="32">32</option>
			<option value="33">33</option>
			<option value="34">34</option>
			<option value="35">35</option>
			<option value="36">36</option>
			<option value="37">37</option>
			<option value="38">38</option>
			<option value="39">39</option>
			<option value="40">40</option>
			<option value="41">41</option>
			<option value="42">42</option>
			<option value="43">43</option>
			<option value="44">44</option>
			<option value="45">45</option>
			<option value="46">46</option>
			<option value="47">47</option>
			<option value="48">48</option>
			<option value="49">49</option>
			<option value="50">50</option>
			<option value="51">51</option>
			<option value="52">52</option>
			<option value="53">53</option>
			<option value="54">54</option>
			<option value="55">55</option>
			<option value="56">56</option>
			<option value="57">57</option>
			<option value="58">58</option>
			<option value="59">59</option>
		</select>

		<select name="seconds" id="seconds">
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			<option value="6">6</option>
			<option value="7">7</option>
			<option value="8">8</option>
			<option value="9">9</option>
			<option value="10">10</option>
			<option value="11">11</option>
			<option value="12">12</option>
			<option value="13">13</option>
			<option value="14">14</option>
			<option value="15">15</option>
			<option value="16">16</option>
			<option value="17">17</option>
			<option value="18">18</option>
			<option value="19">19</option>
			<option value="20">20</option>
			<option value="21">21</option>
			<option value="22">22</option>
			<option value="24">24</option>
			<option value="25">25</option>
			<option value="26">26</option>
			<option value="27">27</option>
			<option value="28">28</option>
			<option value="29">29</option>
			<option value="30">30</option>
			<option value="31">31</option>
			<option value="32">32</option>
			<option value="33">33</option>
			<option value="34">34</option>
			<option value="35">35</option>
			<option value="36">36</option>
			<option value="37">37</option>
			<option value="38">38</option>
			<option value="39">39</option>
			<option value="40">40</option>
			<option value="41">41</option>
			<option value="42">42</option>
			<option value="43">43</option>
			<option value="44">44</option>
			<option value="45">45</option>
			<option value="46">46</option>
			<option value="47">47</option>
			<option value="48">48</option>
			<option value="49">49</option>
			<option value="50">50</option>
			<option value="51">51</option>
			<option value="52">52</option>
			<option value="53">53</option>
			<option value="54">54</option>
			<option value="55">55</option>
			<option value="56">56</option>
			<option value="57">57</option>
			<option value="58">58</option>
			<option value="59">59</option>
		</select><br>
</form>
		<button class="start-timer" id="startTimer">Start Timer</button>


	</div>
<script>
var hours = 0;
var minutes = 0;
var seconds = 0;
var interval = null;

document.getElementById('hours').addEventListener('change', e => {
    hours = +e.target.value;
});

document.getElementById('minutes').addEventListener('change', e => {
    minutes = +e.target.value;
});

document.getElementById('seconds'). addEventListener('change',e => {
    seconds = +e.target.value;
});

document.getElementById('startTimer').addEventListener('click', () => {
    var timeInSeconds = (hours * 60 * 60) +
        (minutes * 60) +
        seconds;

    var displayTime = () => {
        var displayHours = Math.floor(timeInSeconds / (60 * 60));
        var remainder = timeInSeconds - (displayHours * 60 * 60);
        var displayMinutes = Math.floor(remainder / 60);
        var displaySeconds = remainder - (displayMinutes * 60);
        document.getElementById("timer").innerHTML = displayHours + " : " + displayMinutes + " : " + displaySeconds;
    };
    interval = setInterval(() => {
        displayTime();
        timeInSeconds -= 1;
        if (timeInSeconds < 0) {
            clearInterval(interval);
        }
    }, 1000);
});
</script>
<br><br><br>

<footer class="footer">
      <table><tr>
           
        <div class="footer1"> <h4>Pour nous contacter</h4> </div>
            
                <div class="footer2"> Par Courrier : 2 Rue du Miam  00000 Miamville</div>
               <div class="footer3"> Par E-mail : helloworld@gmail.com</div>
              <div class="footer4">  Par Telephone : 03 40 35 30 25</div>
              <div class="footer5">   Nos réseaux sociaux :
                <a href="https://fr-fr.facebook.com/"><img width="20" height="20" src="https://cdn.pixabay.com/photo/2015/05/17/10/51/facebook-770688_640.png" alt=""></a>
              <a href="https://fr.linkedin.com/"><img width="20" height="20"src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/LinkedIn_logo_initials.png/600px-LinkedIn_logo_initials.png" alt=""></a>
              
<a href="https://twitter.com/?lang=fr"><img width="20" height="20"src="https://f.hellowork.com/blogdumoderateur/2019/11/twitter-logo.jpg" alt=""></a>
            </div> 
       
         </div> 
        </tr></table></footer>
</body>
</html>