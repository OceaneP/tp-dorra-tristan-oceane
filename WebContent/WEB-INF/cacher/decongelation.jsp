<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>decongelation</title>
<link rel="stylesheet" href="style_cuisine.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</head>


<body class="body">
    <header class="header">
            <nav class= "navbar navbar-expand-lg navbar-light bg-light">
            <img class = "img_gauche" width="150px" src="https://i.pinimg.com/originals/b1/9b/79/b19b7971a77bcbe56edcca0bb6dea5dc.jpg" alt="Miam">
           
            <a class="navbar-brand" href="accueil.html">Accueil</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav">
                <li class="nav-item">
                  <a class="nav-link" href="decongelation">Guide de d�cong�lation</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="conversion">Guide de conversion d'unit�s</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="minuteur.jsp">Minuteur</a>
                </li>
                
              </ul>
            </div>           
            <img class = "img_droite" width="150px" src="https://www.feter-recevoir.com/upload/image/20-serviettes-vintage-patisserie-33x33cm-p-image-181854-grande.jpg" alt="Gateaux"> 
            </nav>
    </header>  
    <br>
   <h2>Vous trouverez ici un outil simple calculant le temps de d�cong�lation de vos aliments pr�f�r�es !</h2><br>

<form method="post" autocomplete="on">
   <p><label> La masse en kg <input type='number' name='masse' value="${masse}"></label></p>
    <p><input type="submit">${message}</p>
</form>
<br><br><br>
<footer class="footer">
      <table><tr>
           
        <div class="footer1"> <h4>Pour nous contacter</h4> </div>
            
                <div class="footer2"> Par Courrier : 2 Rue du Miam  00000 Miamville</div>
               <div class="footer3"> Par E-mail : helloworld@gmail.com</div>
              <div class="footer4">  Par Telephone : 03 40 35 30 25</div>
              <div class="footer5">   Nos r�seaux sociaux :
                <a href="https://fr-fr.facebook.com/"><img width="20" height="20" src="https://cdn.pixabay.com/photo/2015/05/17/10/51/facebook-770688_640.png" alt=""></a>
              <a href="https://fr.linkedin.com/"><img width="20" height="20"src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/LinkedIn_logo_initials.png/600px-LinkedIn_logo_initials.png" alt=""></a>
              
<a href="https://twitter.com/?lang=fr"><img width="20" height="20"src="https://f.hellowork.com/blogdumoderateur/2019/11/twitter-logo.jpg" alt=""></a>
            </div> 
       
         </div> 
        </tr></table></footer>

</body>
</html>